<?php

namespace ArthurL;

 class FurniImager {

    private $bgImage = null;
    private $file;
    private $name;
    private $layers;
    private $colors;
    public $data = [];
    private $images = [];

    /**
     * Constructor method
     * @param $file String
     */
    public function __construct($file, $bgImage = null)
    {
        if (!extension_loaded("gd")){
            throw new Exception("GD extension not has loaded");
            return;
        }
        if ($bgImage !== null){
            $this->bgImage = $bgImage;
        }
        $this->ExtractSWF($file);
    }

    /**
     * Function to build furni images based on params.
     * 
     * @param $size Int Define furni size (32 or 64)
     * @param $direc Int Define furni direction
     * @param $color Hex Define furni color
     * @param $bgImage Bool Use background?
     */

    public function image($size=64,$direc=2,$color=0, $bgImage = true){
        try {
            $bg = new \claviska\SimpleImage();
            if ($bgImage && $this->bgImage !== null)
                $bg->fromString(file_get_contents($this->bgImage));
            else
                $bg->fromNew(640,640);
            
            $image = new \claviska\SimpleImage();
            $image->fromNew(250,250);
            $layerId = 0;
            if (sizeof($this->furniImages($size,$direc)) == 0){
                $direc = 0;
            }
            foreach($this->furniImages($size,$direc) as $imagee){
                if (!isset($imagee['resource'])){
                    continue;
                }
                $sm = new \claviska\SimpleImage();
                $sm->fromString($imagee['resource']);
                $x = 0-$imagee['x'] + $sm->getWidth()/2;
                $y = 0-$imagee['y'] + $sm->getHeight()/2;
                $image->overlay($sm, '', 1, $x, $y);
            }
            $bg->overlay($image,'',1,0,15);
            return $bg;
          } catch(Exception $err) {

          }
    }

    /**
     * 
     */
    public function getColor($size="", $id=""){
        if (!$this->hasColors())
            return null;

            $colorLayer = $this->getColors($size)
                ->color[$id]->colorLayer->attributes();
            return [
                (int)$colorLayer->id => (String)$colorLayer->color
            ];
    }

    public function getColors($size="1"){
        if (!$this->hasColors())
            return;

        $visualizations = $this->getData()['visualizationData'];
        foreach ($visualizations as $visualization){
            $attr = $visualization->attributes();
            if ((string)$size == (string)$attr->size){
                return $visualization->colors;
            }
        }
    }

    public function hasColors()
    {
        return isset($this->getData()['visualizationData']->visualization->colors);
    }

    private function furniImages($size=64,$direc=2,$color=0)
    {
        $images64 =[];
        $original_direc = $direc;
        $assets = $this->data['assets']->asset;
        foreach($assets as $asset){
            $attr = $asset->attributes();
            if (strpos($attr['name'], '_'.$size.'_') !== false 
                && strpos($attr['name'], '_'.$direc.'_0') !== false 
                && strpos($attr['name'], '_sd_') == false){
                    $result = [
                    "name" => (String)$attr['name'],
                    "x" => (int)$attr['x'],
                    "y" => (int)$attr['y'],
                    "flip" => isset($attr['flipH'])
                ];
                foreach($this->images as $key => $image){
                    if (!strpos($key, '_'.$size.'_')){
                        continue;
                    }
                    if (isset($attr['flipH'])){
                        if (strpos($key, (String)$attr['source']) !== false){
                            $result['resource'] = $image;
                        }
                    } else {
                        if (strpos($key, (String)$attr['name']) !== false){
                            $result['resource'] = $image;
                        }
                    }
                }
                array_push($images64,$result);
            }
        }
        return $images64;
    }


    private function ExtractSWF($filename)
    {
        $swfID = basename($filename, ".swf");
    
        $raw = file_get_contents($filename);
        
        if($raw === false) return false;
        
        if (substr($raw, 0, 3) == 'CWS') {
            $raw = 'F' . substr($raw, 1, 7) . gzuncompress(substr($raw, 8));
        }
        list(, $file_length) = unpack('V', substr($raw, 4, 4));
        
        $header_length = 8 + 1 + ceil(((ord($raw[8]) >> 3) * 4 - 3) / 8) + 4;
        
        $symbols = [];
        $pngs = [];
        $xmls = [];
        
        for ($cursor = $header_length; $cursor < $file_length; )
        {	
            list(, $tag_header) = unpack('v', substr($raw, $cursor, 2));
            $cursor += 2;
            
            list($tag_code, $tag_length) = [$tag_header >> 6, $tag_header & 0x3f];
            
            if ($tag_length == 0x3f) {
                list(, $tag_length) = unpack('V', substr($raw, $cursor, 4));
                $cursor+= 4;
            }
            
            switch ($tag_code) {
                case 36:
                    if($swfID != "Habbo")
                    {
                        $tag = $this->SWFREAD_PNG(substr($raw, $cursor, $tag_length));
                        $pngs[$tag['symbol_id']] = $tag['im'];
                    }
                    break;
                
                case 76:
                    $symbols = $this->SWFREAD_SYM(substr($raw, $cursor, $tag_length));
                    break;
    
                case 87:
                    $tag = $this->SWFREAD_BIN(substr($raw, $cursor, $tag_length));
                    $xmls[$tag['symbol_id']] = $tag['data'];
                    break;
            }
            
            $cursor += $tag_length;
        }
        
        $cnt = 0;
        $xmls_length = count($xmls);
        foreach ($xmls as $symbol_id => $xml) {
            $cnt++;
            $name = isset($symbols[$symbol_id]) ? $symbols[$symbol_id] : 'symbol_' . $symbol_id;
            $_xml = @simplexml_load_string($xml);
            if(!$_xml) {
                break;
            }
            $_name = $_xml->getName();
            if ($_name !== "objectData" && $_name !== "manifest"
                && $_name !== "visualizationData" && $_name !== "object"
                && $_name !== "assets"){
                break;
            }
            $this->data[$_name] = $_xml;
        }
        
        $cnt = 0;
        $pngs_length = count($pngs);
        foreach ($pngs as $symbol_id => $png) {
            $cnt++;
            $name = isset($symbols[$symbol_id]) ? $symbols[$symbol_id] : 'symbol_' . $symbol_id;
            ob_start();
            imagepng($png);
            $image_data = ob_get_contents();
            ob_end_clean();
            $this->images[$name] = $image_data;
        }
    }

    private function SWFREAD_SYM($raw)
    {
        list(, $number_of_symbols) = unpack('v', substr($raw, 0, 2));
        $raw = substr($raw, 2);
        for ($symbols = []; strlen($raw); ) {
            extract(unpack('vsymbol_id/Z*symbol_value', $raw));
            $symbols[$symbol_id] = $symbol_value;
            $raw = substr($raw, 2 + strlen($symbol_value) + 1);
        }
        return $symbols;
    }
    private function SWFREAD_BIN($raw)
    {
        $length = strlen($raw) - 6;
        return unpack('vsymbol_id/V/Z' . $length . 'data', $raw);
    }
    private function SWFREAD_PNG($raw)
    {
        $tag = unpack('vsymbol_id/Cformat/vwidth/vheight', $raw);
        $data = gzuncompress(substr($raw, 7));
        
        if ($tag['format'] != 5) return [];
        
        $im = imageCreateTrueColor($tag['width'], $tag['height']);
        $rectMask = imageColorAllocateAlpha($im, 255, 0, 255, 127);
        imageFill($im, 0, 0, $rectMask);
        $i = 0;
        for ($y = 0; $y < $tag['height']; $y++) {
            for ($x = 0; $x < $tag['width']; $x++) {
                $alpha = ord($data[$i++]);
                $alpha = 127 - $alpha / 2;
                $red   = ord($data[$i++]);
                $green = ord($data[$i++]);
                $blue  = ord($data[$i++]);
                $color = imageColorAllocateAlpha($im, $red, $green, $blue, $alpha);
                imageSetPixel($im, $x, $y, $color);
            }
        }
        imageSaveAlpha($im, true);
        $tag['im'] = $im;
        return $tag;
    }

    private function HEX2RGB($hex)
	{
		$rgb = array();
		for ($x=0;$x<3;$x++){
			$rgb[$x] = hexdec(substr($hex,(2*$x),2));
		}
		return $rgb;
	}

    private function setPartColor(&$resource, $color, $lite = false)
	{
		$replaceColor = $this->HEX2RGB($color);
		if($lite)
		{
            imageFilter($resource, IMG_FILTER_COLORIZE, $replaceColor[0]-255, $replaceColor[1]-255, $replaceColor[2]-255);
            return true;
        }
        
        $width = imageSX($resource);
        $height = imageSY($resource);
        for($y = 0; $y < $height; $y++)
        {
            for($x = 0; $x < $width; $x++)
            {
                $rgb = imageColorsForIndex($resource, imageColorAt($resource, $x, $y));
                $nr = max(round($rgb['red']		* $replaceColor[0] / 255), 0);
                $ng = max(round($rgb['green']	* $replaceColor[1] / 255), 0);
                $nb = max(round($rgb['blue']	* $replaceColor[2] / 255), 0);
                imageSetPixel($resource, $x, $y, imageColorAllocateAlpha($resource, $nr, $ng, $nb, $rgb['alpha']));
            }
        }
		return true;
    }
}
